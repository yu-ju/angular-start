import {Component} from '@angular/core';

@Component({
    selector: 'my-app',
    templateUrl: 'templates/app.html',
    styleUrls: ['styles/app.css']
})

export class AppComponent{ }
